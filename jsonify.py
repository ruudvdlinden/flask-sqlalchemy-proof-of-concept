from sqlalchemy.ext.declarative import DeclarativeMeta
from flask import json


class AlchemyEncoder(json.JSONEncoder):
    """Basic encoder filtering out SQL Alchemy fields"""

    def default(self, obj):
        if isinstance(obj.__class__, DeclarativeMeta):
            data = {}
            fields = obj.__json__() if hasattr(obj, '__json__') else dir(obj)
            for field in [f for f in fields if not f.startswith('_') and f not in ['metadata', 'query', 'query_class']]:
                value = obj.__getattribute__(field)
                try:
                    json.dumps(value)
                except TypeError:
                    data[field] = None
                else:
                    data[field] = value

            return data

        return json.JSONEncoder.default(self, obj)
