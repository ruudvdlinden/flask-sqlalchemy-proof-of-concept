import uuid

from flask import Flask, make_response, jsonify, request
from flask.views import MethodView

from database import db_session
from jsonify import AlchemyEncoder
from models import User

app = Flask(__name__)
app.json_encoder = AlchemyEncoder


def transactional(func):
    """Decorator to roll back API calls when an exception occurs"""

    def function_wrapper(*args, **kwargs):
        try:
            result = func(*args, **kwargs)
            db_session.commit()
            return result
        except Exception:
            db_session.rollback()

    return function_wrapper


class UserAPI(MethodView):
    @transactional
    def get(self):
        users = User.query.all()

        return make_response(jsonify(users))

    @transactional
    def post(self):
        """Demo method just to quickly create some users"""

        unique = str(uuid.uuid4())
        user = User(name=unique)
        db_session.add(user)

        return make_response(jsonify({'new_user_name': unique}))


app.add_url_rule('/user', view_func=UserAPI.as_view('counter'))
